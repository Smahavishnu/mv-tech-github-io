# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
import random

# Create your views here.

def home(request):
    return render(request,'generator/index.html')

# def about(request):
#     return

def password(request):
    # Characters = list('abcdefghijklmnopqrstuvwxyz')
    #
    # length= 10
    #
    # thepassword=''
    # for x in range(length):
    # thepassword += random.choice(characters)
    #
    # return render(request,'generator/password.html',{'password':thepassword})
